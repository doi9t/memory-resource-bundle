/*
 *    Copyright 2014 - 2020 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.utils.bundle;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * A simple `java.util.ResourceBundle` implementation that uses the `java.lang.ClassLoader.getResourceAsStream` method to load the properties.
 * This implementation DO NOT use any cache, except from the inner `java.util.Properties`, that the values are loaded into.
 * This implementation can be good if the resources files are already loaded in the memory and don’t have a physical file or `java.net.URL`.
 * If not provided, the default locale is {@link Locale#ENGLISH}, and we don’t check for the <i>extended<i/> format (will return null faster)
 * <h1>Properties format reading order<h1/>
 * <ol>
 * 	<li>baseName_language</li>
 * 	<li>baseName_language_country</li>
 * 	<li>baseName_language_script <i>(extended)<i/></li>
 * 	<li>baseName_language_country_variant <i>(extended)<i/></li>
 * 	<li>baseName_language_script_country <i>(extended)<i/></li>
 * 	<li>baseName_language_script_country_variant <i>(extended)<i/></li>
 * </ol>
 */
public class StreamReadingResourceBundle extends ResourceBundle {
    public static final String PROPERTY_BASE_FILE_TEMPLATE = "%s_%s.properties";
    public static final String PROPERTY_BASE_FILE_TEMPLATE_WITH_LEADING_SLASH = String.format("/%s", PROPERTY_BASE_FILE_TEMPLATE);
    private static final byte[] EMPTY_ARRAY = {};
    private final Properties properties;
    private final String baseName;
    private final boolean useExtendedFilePatternCheck;
    private ClassLoader classLoader;


    /**
     * This will use the default classloader to load the resources from the stream.
     *
     * @param baseName - The base name of the bundle
     */
    public StreamReadingResourceBundle(String baseName) {
        this(baseName, false, Locale.ENGLISH, StreamReadingResourceBundle.class.getClassLoader());
    }

    /**
     * @param baseName                    - The base name of the bundle
     * @param useExtendedFilePatternCheck - If enabled, we will check for the other bundle file format, when the others are not found.
     * @param locale                      - The locale
     * @param classLoader                 - The classloader, that will be used to load the resources files, with {@link ClassLoader#getResourceAsStream(java.lang.String)}
     */
    public StreamReadingResourceBundle(String baseName, boolean useExtendedFilePatternCheck, Locale locale, ClassLoader classLoader) {
        if (baseName == null || baseName.isEmpty()) {
            throw new IllegalArgumentException("The base name cannot be empty!");
        } else if (locale == null) {
            throw new IllegalArgumentException("The local cannot be null!");
        } else if (classLoader == null) {
            throw new IllegalArgumentException("The classloader cannot be null!");
        }

        this.classLoader = classLoader;
        this.baseName = baseName;
        this.useExtendedFilePatternCheck = useExtendedFilePatternCheck;
        this.properties = new Properties();

        loadProperties(locale);
    }

    private synchronized void loadProperties(Locale locale) {
        properties.clear();

        String language = locale.getLanguage();
        String script = locale.getScript();
        String country = locale.getCountry();
        String variant = locale.getVariant();

        InputStream inputStreamFromOptions = getInputStream(language, script, country, variant);

        if (inputStreamFromOptions == null) {
            throw new MissingResourceException("Cannot find the specified bundle!", StreamReadingResourceBundle.class.getName(), baseName);
        }

        try {
            injectIntoProperties(inputStreamFromOptions);
        } catch (IOException e) {
            throw new MissingResourceException("Cannot load the bundle!", StreamReadingResourceBundle.class.getName(), baseName);
        }
    }

    private InputStream getInputStream(String language, String script, String country, String variant) {
        InputStream languageOnly = getResourceAsStreamFromLanguageSection(language);
        if (languageOnly != null) {
            return languageOnly;
        }

        InputStream languageCountry = getResourceAsStreamFromLanguageSection(buildLocalePart(language, country));
        if (languageCountry != null) {
            return languageCountry;
        }

        if (useExtendedFilePatternCheck) {
            InputStream languageScript = getResourceAsStreamFromLanguageSection(buildLocalePart(language, script));
            if (languageScript != null) {
                return languageScript;
            }

            InputStream languageCountryVariant = getResourceAsStreamFromLanguageSection(buildLocalePart(language, country, variant));
            if (languageCountryVariant != null) {
                return languageCountryVariant;
            }

            InputStream languageScriptCountry = getResourceAsStreamFromLanguageSection(buildLocalePart(language, script, country));
            if (languageScriptCountry != null) {
                return languageScriptCountry;
            }

            return getResourceAsStreamFromLanguageSection(buildLocalePart(language, script, country, variant));
        }

        return null;
    }

    private void injectIntoProperties(InputStream inputStream) throws IOException {
        properties.load(inputStream);
    }

    private InputStream getResourceAsStreamFromLanguageSection(String languageSection) {
        InputStream resourceAsStream = classLoader.getResourceAsStream(String.format(PROPERTY_BASE_FILE_TEMPLATE, baseName, languageSection));

        if (resourceAsStream != null) {
            return resourceAsStream;
        }

        // Retry with a leading slash, in case the classloader need it.
        return classLoader.getResourceAsStream(String.format(PROPERTY_BASE_FILE_TEMPLATE_WITH_LEADING_SLASH, baseName, languageSection));
    }

    private String buildLocalePart(String... parts) {
        if (parts == null || parts.length == 0) {
            return "";
        }

        List<String> filtered = new ArrayList<>(parts.length);

        for (String value : parts) {
            if (value == null || value.isEmpty()) {
                continue;
            }

            filtered.add(value);
        }

        return String.join("_", filtered);
    }

    /**
     * @param baseName    - The base name of the bundle
     * @param classLoader - The classloader, that will be used to load the resources files, with {@link ClassLoader#getResourceAsStream(java.lang.String)}
     */
    public StreamReadingResourceBundle(String baseName, ClassLoader classLoader) {
        this(baseName, false, Locale.ENGLISH, classLoader);
    }

    /**
     * @param baseName    - The base name of the bundle
     * @param locale      - The locale
     * @param classLoader - The classloader, that will be used to load the resources files, with {@link ClassLoader#getResourceAsStream(java.lang.String)}
     */
    public StreamReadingResourceBundle(String baseName, Locale locale, ClassLoader classLoader) {
        this(baseName, false, locale, classLoader);
    }

    /**
     * @param baseName                    - The base name of the bundle
     * @param useExtendedFilePatternCheck - If enabled, we will check for the other bundle file format, when the others are not found.
     * @param classLoader                 - The classloader, that will be used to load the resources files, with {@link ClassLoader#getResourceAsStream(java.lang.String)}
     */
    public StreamReadingResourceBundle(String baseName, boolean useExtendedFilePatternCheck, ClassLoader classLoader) {
        this(baseName, useExtendedFilePatternCheck, Locale.ENGLISH, classLoader);
    }

    /**
     * This uses an classloader that returns an empty {@link java.io.ByteArrayInputStream}; this can be useful when the classloader is not ready / loaded and can use the {@link ca.watier.utils.bundle.StreamReadingResourceBundle#setClassLoader} when ready.
     *
     * @param baseName - The base name of the bundle
     */
    public static StreamReadingResourceBundle empty(String baseName) {
        return new StreamReadingResourceBundle(baseName, false, Locale.ENGLISH, new ClassLoader() {
            @Override
            public InputStream getResourceAsStream(String name) {
                return new ByteArrayInputStream(EMPTY_ARRAY);
            }
        });
    }

    @Override
    protected Object handleGetObject(String key) {
        return properties.get(key);
    }

    @Override
    public Enumeration<String> getKeys() {
        return Collections.enumeration(properties.stringPropertyNames());
    }

    public Map<String, String> getBindings() {
        return Collections.unmodifiableMap((Map) properties);
    }

    public ClassLoader getClassLoader() {
        return classLoader;
    }

    public void setClassLoader(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    /**
     * Set the {@link Locale} for the bundle, the default is {@link Locale#ENGLISH}
     *
     * @param locale - The locale
     */
    public void setLocale(Locale locale) {
        loadProperties(locale);
    }
}
