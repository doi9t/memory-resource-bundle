/*
 *    Copyright 2014 - 2020 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.utils.bundle;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;

public class StreamReadingResourceBundleTest {

    private StreamReadingResourceBundle inMemoryResourceBundle;

    @Before
    public void setUp() {
        inMemoryResourceBundle = new StreamReadingResourceBundle("testBundle", false, new ClassLoader() {
            @Override
            public InputStream getResourceAsStream(String name) {
                return StreamReadingResourceBundleTest.class.getResourceAsStream(name);
            }
        });
    }

    @Test(expected = MissingResourceException.class)
    public void key_not_existing_english() {
        // when
        String value = inMemoryResourceBundle.getString("ddfgfg");

        // then
        Assertions.assertThat(value).isEqualTo("value");
    }

    @Test
    public void getString_default_english() {
        // when
        String value = inMemoryResourceBundle.getString("value");

        // then
        Assertions.assertThat(value).isEqualTo("value");
    }

    @Test
    public void getString_english() {
        // given
        inMemoryResourceBundle.setLocale(Locale.ENGLISH);

        // when
        String value = inMemoryResourceBundle.getString("value");


        // then
        Assertions.assertThat(value).isEqualTo("value");
    }

    @Test
    public void getString_french() {
        // given
        inMemoryResourceBundle.setLocale(Locale.FRENCH);

        // when
        String value = inMemoryResourceBundle.getString("value");

        // then
        Assertions.assertThat(value).isEqualTo("valeur");
    }

    @Test
    public void getBindings_english() {
        // given
        Map<String, String> givenMap = Collections.singletonMap("value", "value");


        // when
        Map<String, String> value = inMemoryResourceBundle.getBindings();

        // then
        Assertions.assertThat(value).isEqualTo(givenMap);
    }

    @Test
    public void getBindings_french() {
        // given
        inMemoryResourceBundle.setLocale(Locale.FRENCH);
        Map<String, String> givenMap = Collections.singletonMap("value", "valeur");

        // when
        Map<String, String> value = inMemoryResourceBundle.getBindings();

        // then
        Assertions.assertThat(value).isEqualTo(givenMap);
    }

}