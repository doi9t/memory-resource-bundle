# memory-resource-bundle

A simple `java.util.ResourceBundle` implementation that uses the `java.lang.ClassLoader.getResourceAsStream` method to load the properties.

This implementation DO NOT use any cache, except from the inner `java.util.Properties`, that the values are loaded into.

This implementation can be good if the resources files are already loaded in the memory and don’t have a physical file or `java.net.URL`.